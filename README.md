# Hashtree

Custom Hash-tree implementation using wildcard for finding hierarchical conflicts which can be used for complex validation needs.

## Use Cases: 

1. Can be used for storing configuration in hierarchical structure.
2. Can be used for finding duplicate keys from heirarchical data based on its set function boolean result.
3. Advance conflicts detection using wildcard


## Publish Instruction

## Usage Instruction

1. Create .npmrc file in your project root folder and add below lines in it

```sh
@freightbro:registry=https://gitlab.com/api/v4/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=${GITLAB_TOKEN}
```

2. Set GITLAB_TOKEN environment variable. You can get your access token from this [link](https://gitlab.com/profile/personal_access_tokens). 
After setting environment variable run below command in your terminal to verify the environment variable output.

```sh
echo $GITLAB_TOKEN
```

3. Install the package

```sh
npm install @freightbro/hashtree
```

4. Code Example

```js
const { Hashtree } = require("@freightbro/hashtree");
const hashtree = new Hashtree();
hashtree.set("a.b.c", 1);
let v = hashtree.get("a.b.c");
console.log(v); // should print 1
```

## Usage Instruction

1. Create .npmrc file in your project root folder and add below lines in it

```
@freightbro:registry=https://gitlab.com/api/v4/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=${GITLAB_TOKEN}
```

2. Set GITLAB_TOKEN environment variable. You can get your access token from this [link](https://gitlab.com/profile/personal_access_tokens)

3. Install the package

```
npm install @freightbro/hashtree
```

4. Code Example

```js
const { Hashtree } = require("@freightbro/hashtree");
const hashtree = new Hashtree();
hashtree.set("a.b.c", 1);
let v = hashtree.get("a.b.c");
console.log(v); // should print 1
```
