"use strict";

// For making data property as private within hashtree
let data = Symbol();
let wildKeys = Symbol();

/**
 * Hashtree datastructure implementation. This implementation is done based on 
 * Composite Design Pattern.
 * 
 * Use Cases: 
 * 1. Can be used for storing configuration in hierarchical structure.
 * 2. Can be used for finding duplicate keys from heirarchical data based on its set function boolean result.
 * 3. Advance conflicts detection using wildcard
 * 
 * @author Karthik Elayaperumal
 * @since v1.0.0
 */
export class Hashtree {

    protected delimiter: string;
    protected wildcard: string;


    /**
     * Default constructor of the class.
     * @param {String} delimiter Default value would be .(dot). Delimiter for building hashtree
     */
    constructor(delimiter = '.', wildcard = '*') {
        this.delimiter = delimiter;
        this.wildcard = wildcard;
        (<any>this)[data] = {};
        (<any>this)[wildKeys] = [];
    }

    /**
     * Sets value in hashtree
     * @param {String} key String for building the heirarchy with delimiters.
     * @param {Object} value Scalar value which needs to be stored against hash tree.
     * @param {Boolean} overwrite Scalar value which needs to be stored against hash tree.
     * @returns {Boolean} If value is set in hashtree then true will be returned else false will be returned.
     */
    set(key: string, value: any, overwrite: boolean = false): boolean {
        if (key === undefined || value === undefined) {
            return false;
        }
        let keys: string[] = key.split(this.delimiter);
        let lastKey: string = keys.pop() ?? "";
        let self = <any>this;
        let tmp = self[data];
        for (let k in keys) {
            k = keys[k];
            if (tmp[k] && !isObject(tmp[k])) {
                return false;
            }
            if (!tmp[k]) {
                tmp[k] = {};
            }
            if (hasOwnProperty.call(tmp, k)) {
                tmp = tmp[k];
            }
        }
        if (!overwrite && !isObject(value) && (isScalar(tmp[lastKey]) || isObject(tmp[lastKey]))) {
            return false
        }
        tmp[lastKey] = value;
        if (key.indexOf(this.wildcard) >= 0 && self[wildKeys].indexOf(key) < 0) {
            self[wildKeys].push(key);
        }
        return true;
    }

    /**
     * Retrieves value from hashtree for specified key.
     * @param {String} key 
     * @returns {Object} Either value or subtree.
     */
    get(key: string): any {
        let self = <any>this;
        let tmp = self[data];
        let keys = key.split(this.delimiter);

        if (keys === undefined) {
            return null;
        }

        for (let k in keys) {
            if (typeof tmp === 'object' && hasOwnProperty.call(tmp, keys[k])) {
                tmp = tmp[keys[k]];
            } else {
                tmp = undefined;
                break;
            }
        }
        if (tmp && isObject(tmp)) {
            let subTree = new Hashtree(this.delimiter);
            (<any>subTree)[data] = tmp;
            return subTree;
        }
        return tmp;
    }

    /**
     * Removes the key from hashtree
     * @param {String} key Key to be removed from hashtree.
     * @returns {Boolean} If element removed from hashtree then true will be returned else false.
     */
    remove(key: string): boolean {
        let self = <any>this;
        let tmp, lastKey;
        let keys = key.split(this.delimiter);
        if (keys) {
            lastKey = keys.pop() ?? "";
            tmp = this.get(keys.join(this.delimiter));
            if (tmp && tmp[data][lastKey]) {
                delete tmp[data][lastKey];
                return true;
            } else if (self[data][key]) {
                delete self[data][key];
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves conflicting keys based on wildcards in its tree path.
     * The result would be two dimensional array.
     */
    conflicts(): Array<any> {
        let result = [];
        let self = <any>this;
        if (self[wildKeys]) {
            for (let i in self[wildKeys]) {
                let wk = self[wildKeys][i];
                let wkKeys = wk.split(this.delimiter);
                let baseKeyLastIndex = wk.indexOf(this.wildcard) - 1;
                let baseKey = wk.substring(0, baseKeyLastIndex);
                let baseLevel = baseKey.split(this.delimiter).length;
                let subTree = this.get(baseKey);
                if (subTree) {
                    let keys = Object.keys(subTree[data]);
                    for (let j in keys) {
                        let key = keys[j];
                        let q = [];
                        if (isObject(subTree.get(key))) {
                            q.push(subTree.get(key));
                            let levels = [baseLevel];
                            let level = baseLevel;
                            let baseKeys = [key];
                            while (q.length > 0) {
                                let childTree = q.shift();
                                level = levels.shift();
                                let baseKey2 = baseKeys.shift();
                                let keys2 = Object.keys(childTree[data]);
                                for (let j in keys2) {
                                    let key2 = keys2[j];
                                    if (wkKeys[level + 1] != this.wildcard && wkKeys[level + 1] != key2 && key2 != this.wildcard) {
                                        continue;
                                    }
                                    if (isScalar(childTree.get(key2)) && (wkKeys[level + 1] == this.wildcard || wkKeys[level + 1] == key2)) {
                                        let conflicts = [];
                                        let leafKey = [baseKey, baseKey2, key2].join(this.delimiter);
                                        if (wk != leafKey) {
                                            conflicts.push(wk);
                                            conflicts.push(leafKey);
                                            result.push(conflicts);
                                        }
                                    } else if (isObject(childTree.get(key2))) {
                                        levels.push(level + 1);
                                        q.push(childTree.get(key2));
                                        baseKeys.push([baseKey2, key2].join(this.delimiter));
                                    }
                                }
                            }
                        } else {
                            if (key != this.wildcard && isScalar(subTree.get(key)) && (wkKeys[baseLevel] == this.wildcard || wkKeys[baseLevel] == key)) {
                                let conflicts = [];
                                conflicts.push(wk);
                                conflicts.push([baseKey, key].join(this.delimiter));
                                result.push(conflicts);
                            }
                        }
                    }
                }
            }
        }
        return result;

    }
}

/**
 * Checks if given parameter type is object or not. 
 * If argument type is object true is returned else false.
 * @param {Object} o 
 * @returns {Boolean} True if argument type is object else false.
 */
function isObject(o: any) {
    return typeof o == 'object';
}

/**
 * Checks if given parameter type is scalar type (string, number, boolean).
 * If argument type is scalar true will be returned else false.
 * @param {*} o 
 * @returns {Boolean} True if argument type is scalar value else false.
 */
function isScalar(o: any) {
    return ['string', 'number', 'boolean'].indexOf(typeof o) >= 0;
}

/**
 * Function short hand reference
 */
let hasOwnProperty = Object.prototype.hasOwnProperty;