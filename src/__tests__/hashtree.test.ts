import { expect } from "chai";
import { Hashtree } from "..";


test("set should return true on passing new key", () => {
    const hashTree = new Hashtree();
    const result = hashTree.set("a.b", "1");
    expect(result).to.be.equal(true);
})

test("set should return false on passing duplicate key", () => {
    const hashTree = new Hashtree();
    let result = hashTree.set("a.b", "1");
    expect(result).to.be.equal(true);
    result = hashTree.set("a.b", "2");
    expect(result).to.be.equal(false);
})

test("set should return true on overwriting duplicate key", () => {
    const hashTree = new Hashtree();
    let result = hashTree.set("a.b", "1");
    expect(result).to.be.equal(true);
    result = hashTree.set("a.b", "2", true);
    expect(result).to.be.equal(true);
})

test("set should return false on passing undefined value", () => {
    const hashTree = new Hashtree();
    const result = hashTree.set("a.b", undefined);
    expect(result).to.be.equal(false);
})

test("set should return false if path has scalar value in its path", () => {
    const hashTree = new Hashtree();
    let result = hashTree.set("a.b", "1");
    expect(result).to.be.equal(true);
    result = hashTree.set("a.b.c", "2");
    expect(result).to.be.equal(false);
})

test("set should return true if path has scalar value in its path", () => {
    const hashTree = new Hashtree();
    let result = hashTree.set("a.b", {});
    expect(result).to.be.equal(true);
    result = hashTree.set("a.b.c", "2");
    expect(result).to.be.equal(true);
})

test("get should return value assigned on set for specified key", () => {
    const hashTree = new Hashtree();
    const key1 = "a.b.c";
    const val1 = "1";
    const key2 = "a.b.d";
    const val2 = "2";
    hashTree.set(key1, val1);
    hashTree.set(key2, val2);
    let result = hashTree.get(key1);
    expect(result).to.be.equal(val1);
    result = undefined;
    result = hashTree.get(key2);
    expect(result).to.be.equal(val2);
})

test("get should return undefined for non-existing key", () => {
    const hashTree = new Hashtree();
    const result = hashTree.get("a.b.c");
    expect(result).to.be.equal(undefined);
})

test("get should return child hashtree on passing partial key", () => {
    const hashTree = new Hashtree();
    const key = "a.b.c";
    const subKey = "a.b";
    const leafKey = "c";
    const val = "1";
    hashTree.set(key, val);
    const subTree = hashTree.get(subKey);
    expect(subTree.constructor.name).to.be.equal(hashTree.constructor.name);
    const result = subTree.get(leafKey);
    expect(result).to.be.equal(val);
})

test("remove should return false on passing non-existing key", () => {
    const hashTree = new Hashtree();
    const key = "a.b.c";
    const result = hashTree.remove(key);
    expect(result).to.be.equal(false);
})

test("remove should return true on passing existing key", () => {
    const hashTree = new Hashtree();
    const key = "a.b.c";
    const val = "1";
    hashTree.set(key, val);
    let result = hashTree.remove(key);
    expect(result).to.be.equal(true);
    result = hashTree.get(key);
    expect(result).to.be.equal(undefined);
})

test("level wise keys deconstion should return proper count and remaining keys", () => {
    const hashTree = new Hashtree();
    const key1 = "a.b.c";
    const key2 = "a.*.c";
    const key3 = "a.*.*";
    const key4 = "a.*.d";
    const key5 = "x.*.d";
    const key6 = "z";
    const val = "1";
    hashTree.set(key1, val);
    hashTree.set(key2, val);
    hashTree.set(key3, val);
    hashTree.set(key4, val);
    hashTree.set(key5, val);
    hashTree.set(key6, val);

    let r = hashTree.remove("x.*.d");
    expect(r).to.be.equal(true);

    r = hashTree.remove("a.*.d");
    expect(r).to.be.equal(true);

    r = hashTree.remove("z");
    expect(r).to.be.equal(true);
})

test("root level key deconstion", () => {
    const hashTree = new Hashtree();
    const key1 = "a.b.c";
    const key2 = "a.b.d";
    const val = "1";
    hashTree.set(key1, val);
    hashTree.set(key2, val);

    const r = hashTree.remove("a");
    expect(r).to.be.equal(true);
})

test("test basic conflicts - 1", () => {
    const hashTree = new Hashtree();
    const keys = [
        { k: "a.b.c", r: true },
        { k: "a.b.c", r: false },
        { k: "d.b.g", r: true },
        { k: "d.e.f", r: true },
        { k: "d.*.*", r: true },
        { k: "a.b.*", r: true },
        { k: "d.b.*", r: true },
    ];
    const val = "1";
    for (const i of keys) {
        const r = hashTree.set(i.k, val);
        expect(r).to.be.equal(i.r);
    }
    let conflicts = hashTree.conflicts();
    expect(conflicts).to.eql([
        ['d.*.*', 'd.b.g'],
        ['d.*.*', 'd.b.*'],
        ['d.*.*', 'd.e.f'],
        ['a.b.*', 'a.b.c'],
        ['d.b.*', 'd.b.g']
    ]);

    hashTree.set("a.b.d", val);
    conflicts = hashTree.conflicts();
    expect(conflicts).to.eql([
        ['d.*.*', 'd.b.g'],
        ['d.*.*', 'd.b.*'],
        ['d.*.*', 'd.e.f'],
        ['a.b.*', 'a.b.c'],
        ['a.b.*', 'a.b.d'],
        ['d.b.*', 'd.b.g']
    ]);
});

test("test basic conflicts - 2", () => {
    const hashTree = new Hashtree();
    const val = "1";
    let r;
    const keys = [
        "a.b.c.*.*.z",
        "a.b.c.x.y.z",
        "a.b.c.x.m.z",
        "a.b.m.*.*.z",
        "a.b.m.*.m.z",
        "a.b.m.x.y.m",
        "a.b.m.x.y.z",
        "a.b.m.x.m.z"
    ];
    for (const i of keys) {
        r = hashTree.set(i, val);
        expect(r).to.be.eql(true);
    }
    const conflicts = hashTree.conflicts();
    expect(conflicts).to.be.eql([
        ['a.b.c.*.*.z', 'a.b.c.x.y.z'],
        ['a.b.c.*.*.z', 'a.b.c.x.m.z'],
        ['a.b.m.*.*.z', 'a.b.m.*.m.z'],
        ['a.b.m.*.*.z', 'a.b.m.x.y.z'],
        ['a.b.m.*.*.z', 'a.b.m.x.m.z'],
        ['a.b.m.*.m.z', 'a.b.m.*.*.z'],
        ['a.b.m.*.m.z', 'a.b.m.x.m.z']
    ]);
});

test("test advanced conflicts - 1", () => {
    const hashTree = new Hashtree();
    const val = "1";
    let r;
    const keys = [
        "a.*.c.*.z.*",
        "a.*.*.*.*.z",
        "a.b.c.x.m.z",
        "a.b.m.*.*.z",
        "a.b.m.*.m.z",
        "a.b.m.x.y.m",
        "a.b.m.x.y.z",
        "a.b.m.x.m.z"
    ];
    for (const i of keys) {
        r = hashTree.set(i, val);
        expect(r).to.be.eql(true);
    }
    const conflicts = hashTree.conflicts();
    expect(conflicts).to.be.eql([
        ['a.*.c.*.z.*', 'a.*.*.*.*.z'],
        ['a.*.*.*.*.z', 'a.b.c.x.m.z'],
        ['a.*.*.*.*.z', 'a.b.m.*.*.z'],
        ['a.*.*.*.*.z', 'a.b.m.*.m.z'],
        ['a.*.*.*.*.z', 'a.b.m.x.y.z'],
        ['a.*.*.*.*.z', 'a.b.m.x.m.z'],
        ['a.b.m.*.*.z', 'a.b.m.*.m.z'],
        ['a.b.m.*.*.z', 'a.b.m.x.y.z'],
        ['a.b.m.*.*.z', 'a.b.m.x.m.z'],
        ['a.b.m.*.m.z', 'a.b.m.*.*.z'],
        ['a.b.m.*.m.z', 'a.b.m.x.m.z']
    ]);
});